import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  const firstFighterIndicator = document.getElementById('left-fighter-indicator');
  const secondFighterIndicator = document.getElementById('right-fighter-indicator');
  const playerOneBaseHealth = firstFighter.health;
  const playerTwoBaseHealth = secondFighter.health;
  let isPlayerOneBlockActive = false;
  let isPlayerTwoBlockActive = false;
  let isPlayerOneCritDisable = false;
  let isPlayerTwoCritDisable = false;
  const healthCoef = 1;


  return new Promise((resolve) => {
    const map = {};

    document.addEventListener('keydown', function(event) {
      map[event.code] = event.type === 'keydown';
      switch (true) {
        case event.code === controls.PlayerOneAttack:
          if (isPlayerOneBlockActive || isPlayerTwoBlockActive) {
            return;
          }
          const playerOneDamage = getDamage(firstFighter, secondFighter) * healthCoef;
          if (updateHealth(secondFighter, secondFighterIndicator, playerOneDamage, playerTwoBaseHealth)) {
            resolve(firstFighter);
          }
          break;
        case controls.PlayerOneCriticalHitCombination.every(key => map[key]):
          if (isPlayerOneBlockActive || isPlayerOneCritDisable) {
            return;
          }
          isPlayerOneCritDisable = true;
          setTimeout(() => {
            isPlayerOneCritDisable = false;
          }, 10000);
          const playerOneCritDamage = getCriticalDamage(firstFighter) * healthCoef;
          if (updateHealth(secondFighter, secondFighterIndicator, playerOneCritDamage, playerTwoBaseHealth)) {
            resolve(firstFighter);
          }
          break;
        case event.code === controls.PlayerTwoAttack:
          if (isPlayerTwoBlockActive || isPlayerOneBlockActive) {
            return;
          }
          const playerTwoDamage = getDamage(secondFighter, firstFighter) * healthCoef;
          if (updateHealth(firstFighter, firstFighterIndicator, playerTwoDamage, playerOneBaseHealth)) {
            resolve(secondFighter);
          }
          break;
        case controls.PlayerTwoCriticalHitCombination.every(key => map[key]):
          if (isPlayerTwoBlockActive || isPlayerTwoCritDisable) {
            return;
          }
          isPlayerTwoCritDisable = true;
          setTimeout(() => {
            isPlayerTwoCritDisable = false;
          }, 10000);
          const playerTwoCritDamage = getCriticalDamage(secondFighter) * healthCoef;
          if (updateHealth(firstFighter, firstFighterIndicator, playerTwoCritDamage, playerOneBaseHealth)) {
            resolve(secondFighter);
          }
          break;
        case event.code === controls.PlayerOneBlock:
          isPlayerOneBlockActive = true;
          break;
        case event.code === controls.PlayerTwoBlock:
          isPlayerTwoBlockActive = true;
          break;
        default:
          return;
      }
    });
    document.addEventListener('keyup', function(event) {
      map[event.code] = event.type === 'keydown';
      switch (event.code) {
        case controls.PlayerOneBlock:
          isPlayerOneBlockActive = false;
          break;
        case controls.PlayerTwoBlock:
          isPlayerTwoBlockActive = false;
          break;
        default:
          return;
      }
    });
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return (damage > 0) ? damage : 0;
}

export function getHitPower(fighter) {
  return fighter.attack * getCriticalHitChance();
}

export function getBlockPower(fighter) {
  return fighter.defense * getDodgeChance();
}

function getCriticalDamage(attacker) {
  return 2 * attacker.attack;
}

function getCriticalHitChance(min = 1, max = 2) {
  return Math.random() * (max - min) + min; // except max value
}

function getDodgeChance(min = 1, max = 2) {
  return Math.random() * (max - min) + min; // except max value
}

function updateHealth(fighter, healthIndicator, value, baseHealth) {
  const indicatorPercentage = (value * 100) / baseHealth;
  fighter.health -= value;
  if (fighter.health <= 0) {
    return true;
  }

  const currentHealthIndicator = isNaN(parseFloat(healthIndicator.style.width)) ? 100 :
    parseFloat(healthIndicator.style.width) > 0 ? parseFloat(healthIndicator.style.width) : 0;
  const newHealthIndicator = (currentHealthIndicator - indicatorPercentage);

  healthIndicator.style.width = newHealthIndicator + '%';
  if (newHealthIndicator < 30 && !healthIndicator.classList.contains('arena___health-bar--low')) {
    healthIndicator.classList.add('arena___health-bar--low');
  }
}
