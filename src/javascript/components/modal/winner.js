import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  showModal({
    title: 'The winner is',
    bodyElement: createModalBody(fighter),
    onClose: () => console.log('---close modal---')
  });
}

function createModalBody(fighter) {
  const bodyElement = createElement({ tagName: 'div', className: 'modal-body' });
  const titleElement = createElement({ tagName: 'h3', className: 'winner' });
  const fighterImage = createFighterImage(fighter);

  titleElement.innerText = fighter.name;

  bodyElement.append(titleElement, fighterImage);

  return bodyElement;
}
